﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace LinkTo1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] data = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve" };
            //ParallelLoopResult plr = Parallel.ForEach<string>(data, s =>
            //{
            //    Console.WriteLine(s);
            //}, new ParallelOptions() {TaskScheduler=TaskScheduler.FromCurrentSynchronizationContext() });

            //if (plr.IsCompleted)
            //{
            //    Console.WriteLine("completed!");
            //}
            //var multiplyBlock = new TransformBlock<int,int>(T1);
            //var subBlock = new TransformBlock<int, int>(T2);
            //var option = new DataflowLinkOptions() { PropagateCompletion = true };
            //multiplyBlock.LinkTo(subBlock,option);
            //multiplyBlock.Post(2);
            Console.ReadKey();
        }
        public static int T1(int x)
        {
            Console.WriteLine($"T1 x={x}");
            return x * 2;
        }
        public static int T2(int x)
        {
            Console.WriteLine($"T2 x={x}");
            return x - 2;
        }
    }
}
