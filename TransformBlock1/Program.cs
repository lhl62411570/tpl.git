﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace TransformBlock1
{
    class Program
    {
        static void Main(string[] args)
        {
            TestDownloadHTML();
            Console.ReadKey();
        }
        public static TransformBlock<string,string> tbUrl = new TransformBlock<string,string>(Trans);
        public static void TestDownloadHTML()
        {
            tbUrl.Post("http://www.baidu.com");

            //System.Threading.Thread.Sleep(10000);
            string baiduHTML = tbUrl.Receive();
            Console.WriteLine(baiduHTML);
            tbUrl.Post("http://www.sina.com.cn");
            string sinaHTML = tbUrl.Receive();
            Console.WriteLine(sinaHTML);
        }
        public static string Trans(string url)
        {
            WebClient webClient = new WebClient();
            string str = webClient.DownloadString(new Uri(url));
            Thread.Sleep(1000);
            return str;
        }
    }
}
