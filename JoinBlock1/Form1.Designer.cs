﻿
namespace JoinBlock1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPost1 = new System.Windows.Forms.Button();
            this.btnPost2 = new System.Windows.Forms.Button();
            this.btnRecevie = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPost1
            // 
            this.btnPost1.Location = new System.Drawing.Point(12, 12);
            this.btnPost1.Name = "btnPost1";
            this.btnPost1.Size = new System.Drawing.Size(89, 40);
            this.btnPost1.TabIndex = 0;
            this.btnPost1.Text = "Post1";
            this.btnPost1.UseVisualStyleBackColor = true;
            this.btnPost1.Click += new System.EventHandler(this.btnPost1_Click);
            // 
            // btnPost2
            // 
            this.btnPost2.Location = new System.Drawing.Point(127, 12);
            this.btnPost2.Name = "btnPost2";
            this.btnPost2.Size = new System.Drawing.Size(89, 40);
            this.btnPost2.TabIndex = 1;
            this.btnPost2.Text = "Post2";
            this.btnPost2.UseVisualStyleBackColor = true;
            this.btnPost2.Click += new System.EventHandler(this.btnPost2_Click);
            // 
            // btnRecevie
            // 
            this.btnRecevie.Location = new System.Drawing.Point(12, 78);
            this.btnRecevie.Name = "btnRecevie";
            this.btnRecevie.Size = new System.Drawing.Size(89, 40);
            this.btnRecevie.TabIndex = 2;
            this.btnRecevie.Text = "Recevie";
            this.btnRecevie.UseVisualStyleBackColor = true;
            this.btnRecevie.Click += new System.EventHandler(this.btnRecevie_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 211);
            this.Controls.Add(this.btnRecevie);
            this.Controls.Add(this.btnPost2);
            this.Controls.Add(this.btnPost1);
            this.Font = new System.Drawing.Font("宋体", 12F);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPost1;
        private System.Windows.Forms.Button btnPost2;
        private System.Windows.Forms.Button btnRecevie;
    }
}

