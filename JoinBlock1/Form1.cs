﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace JoinBlock1
{
    public partial class Form1 : Form
    {
        public static JoinBlock<int, string> joinBlock = new JoinBlock<int, string>();
        public const string DateFormat = "yyyy-MM-dd HH:mm:ss.fff";
        public Form1()
        {
            InitializeComponent();
        }

        private void btnPost1_Click(object sender, EventArgs e)
        {
            joinBlock.Target1.Post(10);
            Console.WriteLine($"{DateTime.Now.ToString(DateFormat)} Out:{joinBlock.OutputCount}");
        }

        private void btnPost2_Click(object sender, EventArgs e)
        {
            joinBlock.Target2.Post("20");
            Console.WriteLine($"{DateTime.Now.ToString(DateFormat)} Out:{joinBlock.OutputCount}");
        }

        private void btnRecevie_Click(object sender, EventArgs e)
        {
            Tuple<int, string> tuple;
            bool ret=joinBlock.TryReceive(out tuple);
            if (ret == true)
            {
                Console.WriteLine($"{DateTime.Now.ToString(DateFormat)} Out:{joinBlock.OutputCount} 【{tuple.Item1} {tuple.Item2}】 {ret}");
            }
        }
    }
}