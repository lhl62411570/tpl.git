﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace BroadcastBlock1
{
    public partial class Form1 : Form
    {
        public const string DateFormate = "yyyy-MM-dd HH:mm:ss.fff";
        public static BroadcastBlock<string> broadcastBlock = new BroadcastBlock<string>((str)=> {
            Console.WriteLine($"回调  处理:【{str}】");
            return str;
        });
        ActionBlock<string> displayBlock = new ActionBlock<string>((i) => 
        {
            Thread.Sleep(TimeSpan.FromSeconds(1));
            Console.WriteLine($"Displayed {DateTime.Now.ToString(DateFormate)} 【{i}】");
        });
        ActionBlock<string> saveBlock =    new ActionBlock<string>((i) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(3));
            Console.WriteLine($"Saved     {DateTime.Now.ToString(DateFormate)} 【{i}】");
        });
        ActionBlock<string> sendBlock =    new ActionBlock<string>((i) =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(9));
            Console.WriteLine($"Sent      {DateTime.Now.ToString(DateFormate)} 【{i}】");
        });
        public Form1()
        {
            InitializeComponent();
            broadcastBlock.LinkTo(displayBlock);
            broadcastBlock.LinkTo(saveBlock);
            broadcastBlock.LinkTo(sendBlock);
        }
        static int clickCount = 0;
        private void btnPost_Click(object sender, EventArgs e)
        {
            string info = $"Post Click:{++clickCount} {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}";
            broadcastBlock.Post(info);
            Console.WriteLine(info);
        }
    }
}
