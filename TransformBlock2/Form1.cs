﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace TransformBlock2
{
    public partial class Form1 : Form
    {
        public static TransformBlock<int,int> tranformBlock = new TransformBlock<int, int>(Print);
        private const string DateFormate = "yyyy-MM-dd HH:mm:ss.fff";
        private class Data
        {
            public int ClickCount { get; set; }
            public int Sum { get; set; }
        }
        public Form1()
        {
            InitializeComponent();
        }
        static int clickCount = 0;
        private void btnPost_Click(object sender, EventArgs e)
        {
            tranformBlock.Post(++clickCount);
            string info = $"clickCount={clickCount} In:{tranformBlock.InputCount} Out:{tranformBlock.OutputCount}";
            Console.WriteLine($"Post:{DateTime.Now.ToString(DateFormate)} {info}");
        }
        private void btnReceive_Click(object sender, EventArgs e)
        {
            //string info = $"In:{tranformBlock.InputCount} Out:{tranformBlock.OutputCount}";
            //string str=tranformBlock.Receive<string>();
            //string xx = "";
            //bool ret=tranformBlock.TryReceive(out xx);
            //Console.WriteLine($"{info} {ret} {xx}");
        }
        private static int Print(int data)
        {
            int sum = 0;
            for(int i=0;i<= data; i++)
            {
                sum += i;
            }
            Thread.Sleep(TimeSpan.FromSeconds(2));//耗时2s
            string info = $"clickCount={data} In:{tranformBlock.InputCount} Out:{tranformBlock.OutputCount} Sum={sum} ";
            Console.WriteLine($"处理:{DateTime.Now.ToString(DateFormate)} {info}");
            return sum;
        }
    }
}
