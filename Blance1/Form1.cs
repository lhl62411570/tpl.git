﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace Blance1
{
    public partial class Form1 : Form
    {
        public static BufferBlock<string> bb = new BufferBlock<string>();
        private const string DateFormat = "yyyy-MM-dd HH:mm:ss.fff";
        public Form1()
        {
            InitializeComponent();
            bb.LinkTo(ab1);
            bb.LinkTo(ab2);
            bb.LinkTo(ab3);

            for (int i = 1; i <= 20; i++)
            {
                Thread.Sleep(10);
                string str = $"【BufferBlock Post:{DateTime.Now.ToString(DateFormat)} 值:{i} bb长度:{bb.Count}】";
                bb.Post(str);
            }
        }
        public static ActionBlock<string> ab1 = new ActionBlock<string>((i) =>
        {
            Thread.Sleep(1000);
            Console.WriteLine($"【ab1】{DateTime.Now.ToString(DateFormat)}  {i} ab1长度:{ab1.InputCount} bb长度:{bb.Count}");
        }
        , new ExecutionDataflowBlockOptions() { BoundedCapacity = 2 });

        public static ActionBlock<string> ab2 = new ActionBlock<string>((i) =>
        {
            Thread.Sleep(2000);
            Console.WriteLine($"【ab2】{DateTime.Now.ToString(DateFormat)}  {i} ab2长度:{ab2.InputCount} bb长度:{bb.Count}");
        }
        , new ExecutionDataflowBlockOptions() { BoundedCapacity = 2 });

        public static ActionBlock<string> ab3 = new ActionBlock<string>((i) =>
        {
            Thread.Sleep(3000);
            Console.WriteLine($"【ab3】{DateTime.Now.ToString(DateFormat)}  {i} ab3长度:{ab3.InputCount} bb长度:{bb.Count}");
        }
        , new ExecutionDataflowBlockOptions() { BoundedCapacity = 2 });
    }
}