﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace TPL1
{
    public class TransformBlock1
    {
        public static TransformBlock<string, string> tbUrl = new TransformBlock<string, string>((url) =>
        {
            Console.WriteLine($"url:{url}");
            WebClient webClient = new WebClient();
            string content = webClient.DownloadString(new Uri(url));
            Console.WriteLine($"content:{content}");
            return webClient.DownloadString(new Uri(url));
        });

        public static void TestDownloadHTML()
        {
            tbUrl.Post("http://www.baidu.com");
            tbUrl.Post("http://www.sina.com.cn");

            string baiduHTML = tbUrl.Receive();
            string sinaHTML = tbUrl.Receive();
        }
    }
}
