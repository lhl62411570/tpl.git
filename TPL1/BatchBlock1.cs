﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace TPL1
{
    public class BatchBlock1
    {
        static BatchBlock<int> bb = new BatchBlock<int>(3);

        static ActionBlock<int[]> ab = new ActionBlock<int[]>((i) =>
        {
            string s = string.Empty;

            foreach (int m in i)
            {
                s += m + " ";
            }
            Console.WriteLine($"Time:{DateTime.Now.ToString(Program.TimeFormat)} {s}");
        });

        public static void TestSync()
        {
            bb.LinkTo(ab);

            for (int i = 0; i < 10; i++)
            {
                System.Threading.Thread.Sleep(1000);
                bb.Post(i);
            }
            bb.Complete();

            Console.WriteLine("Finished post");
        }
    }
}
