﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace TPL1
{
    public class BufferBlock1
    {
        private static BufferBlock<int> m_buffer = new BufferBlock<int>();
        public static void Producer()
        {
            int i = 0;
            while (true)
            {
                i++;
                System.Threading.Thread.Sleep(1000);
                int item = new Random().Next(0, 100);
                m_buffer.Post(item);
                Console.WriteLine($"发送总数：{i} item:{item} count:{m_buffer.Count}");
            }
        }
        public static void Consumer()
        {
            int i = 0;
            while (true)
            {
                i++;
                System.Threading.Thread.Sleep(5000);
                int item = m_buffer.Receive();
                Console.WriteLine($"{i}-time:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} item:{item} count:{m_buffer.Count}");
            }
        }
    }
}
