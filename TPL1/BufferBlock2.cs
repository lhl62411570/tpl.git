﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace TPL1
{
    public class BufferBlock2
    {
        public static BufferBlock<string> bb = new BufferBlock<string>();

        public static ActionBlock<string> ab1 = new ActionBlock<string>((i) =>
        {
            Thread.Sleep(1000);
            Console.WriteLine("ab1 handle data:" + i + " Execute Time:" + DateTime.Now.ToString(Program.TimeFormat));
        }
        ,new ExecutionDataflowBlockOptions() { BoundedCapacity = 2 });

        public static ActionBlock<string> ab2 = new ActionBlock<string>((i) =>
        {
            Thread.Sleep(2000);
            Console.WriteLine("ab2 handle data:" + i + " Execute Time:" + DateTime.Now.ToString(Program.TimeFormat));
        }
        ,new ExecutionDataflowBlockOptions() { BoundedCapacity = 4 });

        public static ActionBlock<string> ab3 = new ActionBlock<string>((i) =>
        {
            Thread.Sleep(1000);
            Console.WriteLine("ab3 handle data:" + i + " Execute Time:" + DateTime.Now.ToString(Program.TimeFormat));
        }
        ,new ExecutionDataflowBlockOptions() { BoundedCapacity = 2 });

        public static void Test()
        {
            bb.LinkTo(ab1);
            bb.LinkTo(ab2);
            bb.LinkTo(ab3);

            for (int i = 0; i < 9; i++)
            {
                System.Threading.Thread.Sleep(1);
                string str = $"{i} {DateTime.Now.ToString(Program.TimeFormat)}";
                bb.Post(str);
            }
        }
    }
}
