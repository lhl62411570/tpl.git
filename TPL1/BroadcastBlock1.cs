﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace TPL1
{
    public class BroadcastBlock1
    {
        static BroadcastBlock<int> bb = new BroadcastBlock<int>((i) => { return i; });
        static Stopwatch sw1 = Stopwatch.StartNew();
        static Stopwatch sw2 = Stopwatch.StartNew();
        static Stopwatch sw3 = Stopwatch.StartNew();

        static ActionBlock<int> displayBlock = new ActionBlock<int>((i) =>
        {
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine($"Time:{DateTime.Now.ToString(Program.TimeFormat)} 长度:{displayBlock.InputCount} 耗时:{sw1.Elapsed.TotalSeconds} Displayed:{i}");
        });

        static ActionBlock<int> saveBlock = new ActionBlock<int>((i) =>
        {
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine($"Time:{DateTime.Now.ToString(Program.TimeFormat)} 长度:{displayBlock.InputCount} 耗时:{sw2.Elapsed.TotalSeconds}  Saved:{i}");
        });

        static ActionBlock<int> sendBlock = new ActionBlock<int>((i) =>
        {
            System.Threading.Thread.Sleep(3000);
            Console.WriteLine($"Time:{DateTime.Now.ToString(Program.TimeFormat)} 长度:{displayBlock.InputCount} 耗时:{sw3.Elapsed.TotalSeconds}  Send:{i}");
        });

        public static void TestSync()
        {
            bb.LinkTo(displayBlock);
            bb.LinkTo(saveBlock);
            bb.LinkTo(sendBlock);

            for (int i = 0; i < 40; i++)
            {
                bb.Post(i);
            }
            Console.WriteLine("Post finished");
        }
        public static void TestSync1()
        {
            for (int i = 0; i < 4; i++)
            {
                bb.Post(i);
            }

            Thread.Sleep(5000);

            bb.LinkTo(displayBlock);
            bb.LinkTo(saveBlock);
            bb.LinkTo(sendBlock);
            Console.WriteLine("Post finished");
        }
    }
}
