﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace TPL1
{
    public class ActionBlock1
    {
        private static ActionBlock<int> actionBlock = new ActionBlock<int>((x)=> {
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine($"长度:{actionBlock.InputCount} Time:{DateTime.Now.ToString(Program.TimeFormat)} 值:{x}");
        });

        public static void Producer()
        {
            while (true)
            {
                System.Threading.Thread.Sleep(1000);
                int rnd = new Random().Next(0, 100);
                actionBlock.Post(rnd);
            }
        }
    }
}
