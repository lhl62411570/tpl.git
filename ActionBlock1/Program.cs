﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace ActionBlock1
{
    class Program
    {
        static ActionBlock<string> actionBlock = new ActionBlock<string>((str)=> {
            Thread.Sleep(1000);
            Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} Tid:{Thread.CurrentThread.ManagedThreadId} 长度:{actionBlock.InputCount} 【{str}】");
        }, new ExecutionDataflowBlockOptions() { MaxDegreeOfParallelism=3 });
        static void Main(string[] args)
        {
            TestSync();
            Console.ReadKey();
        }
        public static void TestSync()
        {
            for (int i = 0; i < 10; i++)
            {
                actionBlock.Post(i.ToString());
            }
            actionBlock.Complete();
            Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}【Post完成】");
            actionBlock.Completion.Wait();
            Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}【队列处理完成】");
        }
        public static void Print(string str)
        {
            //await Task.Delay(1000);
            Thread.Sleep(1000);
            Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} Tid:{Thread.CurrentThread.ManagedThreadId} 长度:{actionBlock.InputCount} 【{str}】");
        }
        public static async Task Print1(string str)
        {
            await Task.Delay(1000);
            Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} Tid:{Thread.CurrentThread.ManagedThreadId} 长度:{actionBlock.InputCount} 【{str}】");
        }
    }
}

