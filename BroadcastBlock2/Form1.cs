﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace BroadcastBlock2
{
    public partial class Form1 : Form
    {
        BroadcastBlock<int> broadcaster = new BroadcastBlock<int>(null);
        public Form1()
        {
            InitializeComponent();
            /*创建一个toggleCheckBoxBlock*/
            var toggleCheckBoxBlock = new ActionBlock<CheckBox>(checkBox =>
            {
                checkBox.Checked = !checkBox.Checked;
                Console.WriteLine($"chk名称：{checkBox.Name}");
            },new ExecutionDataflowBlockOptions
            {
                TaskScheduler = TaskScheduler.FromCurrentSynchronizationContext()
            });

            var taskSchedulerPair = new ConcurrentExclusiveSchedulerPair();
            /*创建三个readerActions，向toggleCheckBoxBlock投递Post数据*/
            var readerActions = from checkBox in new CheckBox[] { checkBox1, checkBox2, checkBox3 }
            select new ActionBlock<int>(milliseconds =>
            {
                toggleCheckBoxBlock.Post(checkBox);
                Thread.Sleep(milliseconds);
                toggleCheckBoxBlock.Post(checkBox);
            },
            new ExecutionDataflowBlockOptions
            {
                TaskScheduler = taskSchedulerPair.ConcurrentScheduler
            });
            /*创建一个writerAction，向toggleCheckBoxBlock投递Post数据*/
            var writerAction = new ActionBlock<int>(milliseconds =>
            {
                toggleCheckBoxBlock.Post(checkBox4);
                Thread.Sleep(milliseconds);
                toggleCheckBoxBlock.Post(checkBox4);
            }, new ExecutionDataflowBlockOptions
            {
                TaskScheduler = taskSchedulerPair.ExclusiveScheduler
            });
            /*链接数据块，三个readerAction，一个writerAction*/
            foreach (var readerAction in readerActions)
            {
                broadcaster.LinkTo(readerAction);
            }
            broadcaster.LinkTo(writerAction);

            timer1.Start();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            broadcaster.Post(1000);
            Console.WriteLine($"broadcaster长度");
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}

