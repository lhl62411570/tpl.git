﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace TransformManyBlock1
{
    public partial class Form1 : Form
    {
        private static TransformManyBlock<int, int> manyBlock = new TransformManyBlock<int, int>(Print);
        private static ActionBlock<int> actionBlock = new ActionBlock<int>((item) => {
            Console.WriteLine(item);
        
        });
        public Form1()
        {
            InitializeComponent();
            //manyBlock.LinkTo(actionBlock);
        }

        private void btnPost_Click(object sender, EventArgs e)
        {
            manyBlock.Post(1000);
        }
        private void btnRecive_Click(object sender, EventArgs e)
        {
            Console.WriteLine(manyBlock.OutputCount);
            int x = 0;
            bool ret=manyBlock.TryReceive(out x);
            Console.WriteLine($"{manyBlock.OutputCount}  {x} {ret}");
            
        }

        public static int[] Print(int i)
        {
            int[] arr = new int[] { i, i + 1 };
            string info = $"In:{manyBlock.InputCount} Out:{manyBlock.OutputCount}";
            Console.WriteLine(info);
            return arr;
           // return null;
        }


    }
}
