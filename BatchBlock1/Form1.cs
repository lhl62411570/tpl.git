﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace BatchBlock1
{
    public partial class Form1 : Form
    {
        public static BatchBlock<int> batchBlock = new BatchBlock<int>(3);
        public const string DateFormate = "yyyy-MM-dd HH:mm:ss.fff";
        public Form1()
        {
            InitializeComponent();
        }
        static int clickCount = 0;
        private void btnPost_Click(object sender, EventArgs e)
        {
            try
            {
                batchBlock.Post(++clickCount);
                Console.WriteLine($"{DateTime.Now.ToString(DateFormate)} " +
                    $"Post  后 Out:{batchBlock.OutputCount} ClickCount:{clickCount}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        private void btnRecive_Click(object sender, EventArgs e)
        {
            Console.WriteLine($"{DateTime.Now.ToString(DateFormate)} " +
                $"Recive前 Out:{batchBlock.OutputCount}");
            int[] data;
            //int[] data = batchBlock.Receive();
            bool ret = batchBlock.TryReceive(out data);
            string str = "";
            if (ret == true)
            {         
                foreach (int item in data)
                {
                    str = str + item.ToString() + ",";
                }
            }
            Console.WriteLine($"{DateTime.Now.ToString(DateFormate)} " +
                $"Recive后 Out:{batchBlock.OutputCount} 【{str.TrimEnd(',')}】 {ret}");
            batchBlock.Complete();
        }
    }
}

