﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace BroadcastBlock3
{
    public partial class Form1 : Form
    {
        public ActionBlock<int> readerAB1;
        public ActionBlock<int> readerAB2;
        public ActionBlock<int> readerAB3;
        public ActionBlock<int> writerAB1;
        public BroadcastBlock<int> bb = new BroadcastBlock<int>((i) => { return i; });
        public Form1()
        {
            InitializeComponent();

        }

        static int clickCount = 0;
        private void btnPost_Click(object sender, EventArgs e)
        {
            //string info = $"Post Click:{++clickCount} {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}";
            //broadcastBlock.Post(info);
            //Console.WriteLine(info);
            Test();
        }
        public void Test()
        {
            ConcurrentExclusiveSchedulerPair pair = new ConcurrentExclusiveSchedulerPair();

            readerAB1 = new ActionBlock<int>((i) =>
            {
                clickCount++;
                Console.WriteLine($"ReaderAB1 begin handling.Execute Time:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} {clickCount}");
                Thread.Sleep(500);
            }
            , new ExecutionDataflowBlockOptions() { TaskScheduler = pair.ConcurrentScheduler });

            readerAB2 = new ActionBlock<int>((i) =>
            {
                clickCount++;
                Console.WriteLine($"ReaderAB2 begin handling.Execute Time:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} {clickCount}");
                Thread.Sleep(500);
            }
            , new ExecutionDataflowBlockOptions() { TaskScheduler = pair.ConcurrentScheduler });

            readerAB3 = new ActionBlock<int>((i) =>
            {
                clickCount++;
                Console.WriteLine($"ReaderAB3 begin handling.Execute Time:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} {clickCount}");
                Thread.Sleep(500);
            }
            , new ExecutionDataflowBlockOptions() { TaskScheduler = pair.ConcurrentScheduler });

            writerAB1 = new ActionBlock<int>((i) =>
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("WriterAB1 begin handling." + " Execute Time:" + DateTime.Now);
                Console.ResetColor();
                Thread.Sleep(3000);
            }
            , new ExecutionDataflowBlockOptions() { TaskScheduler = pair.ExclusiveScheduler });

            bb.LinkTo(readerAB1);
            bb.LinkTo(readerAB2);
            bb.LinkTo(readerAB3);


            Task.Run(() =>
            {
                while (true)
                {
                    bb.Post(1);
                    Thread.Sleep(1000);
                }
            });

            Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(6000);
                    writerAB1.Post(1);
                }
            });
        }
    }
}
